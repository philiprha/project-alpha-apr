from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from tasks.models import Task


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            Task = form.save(commit=False)
            Task.owner = request.user
            Task.save()
            return redirect("project-list")
    else:
        form = TaskForm()

    return render(request, "tasks/create_task.html", {"form": form})


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    return render(request, "tasks/task_list.html", {"tasks": tasks})
