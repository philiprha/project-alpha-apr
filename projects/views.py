from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from projects.forms import ProjectForm

# Create your views here.


@login_required
def project_list(request):
    projects = Project.objects.filter(name=request.user)
    context = {
        "project_list": projects,
    }
    return render(request, "projects/detail.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id, owner=request.user)
    context = {
        "project": project,
    }
    return render(request, "projects/taskdetail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.owner = request.user
            project.save()
            return redirect("project_list")
    else:
        form = ProjectForm()
    return render(request, "projects/create_project.html", {"form": form})
